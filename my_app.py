import os
import json
import logging
import jinja2

from sklearn.externals import joblib
from flask import Flask, request, jsonify

def create_app():
    app = Flask("my_flask_web_app")
    return app

app = create_app()

@app.route("/", methods=['GET'])
def home():
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates')
    ).get_template("home.html").render({})


@app.route("/predict", methods=['GET', 'POST'])
def predict():
    text = request.values['text']  # read input from GET/POST field "text"
    prediction_class_id = pipeline.predict([text])[0]
    prediction_class_name = class_dict[str(prediction_class_id)]
    result = {'prediction': prediction_class_name, 'text': text}
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates')
    ).get_template("predict.html").render(result)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    pipeline = joblib.load(os.path.join('data', 'simple_model.pkl'))
    class_dict = json.load(open(os.path.join('data', 'prediction_dict.json')))
    logging.info("loaded pipeline %s", pipeline)
    app.run(host='127.0.0.1')  # launch Flask's built-in HTTP server on localhost

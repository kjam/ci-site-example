import pytest
from flask import url_for

@pytest.fixture(scope="session")
def app():
    from my_app import app
    return app

def test_my_json_response(app, client):
    res = client.get("/")
    assert res.status_code == 200

def test_my_predict_response(app, client):
    res = client.get("/predict")
    assert res.status_code == 200
